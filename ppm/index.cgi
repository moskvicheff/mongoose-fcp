#!/bin/bash


function error()
{
        echo "<div class=\"notice error\">"
        echo "<span class=\"icon medium\" data-icon=\"L\"></span>" #icon
        msg="${1//%20/ }"
	echo "${msg//%27/\`}"
        echo "<a href=\"#close\" class=\"icon close\" data-icon=\"x\"></a>"
        echo "</div>"
}

function success()
{
        echo "<div class=\"notice success\">"
        echo "<span class=\"icon medium\" data-icon=\"K\"></span>" #icon
        echo "${1//%20/ }"
        echo "<a href=\"#close\" class=\"icon close\" data-icon=\"x\"></a>"
        echo "</div>"
}

function enableppm()
{
	curdate=$(date +%d.%m.%y\ %H:%M)
	/opt/Avaya/bin/start -s mgmt
	if [ $? -ne 0 ]; then
		redirerror "Error!"
  		echo "$curdate Client ip $REMOTE_ADDR ; enable" >> errlog.txt
	else
		redirsuccess "PPM enabled"
		echo "$curdate Client ip $REMOTE_ADDR ; enable" >> log.txt
	fi
}

function disableppm()
{
        curdate=$(date +%d.%m.%y\ %H:%M)
        /opt/Avaya/bin/stop -f -s mgmt
        if [ $? -ne 0 ]; then
                redirerror "Error!"
                echo "$curdate Client ip $REMOTE_ADDR ; disable" >> errlog.txt
        else
                redirsuccess "PPM is shutting DOWN, WAIT!"
                echo "$curdate Client ip $REMOTE_ADDR ; disable" >> log.txt
        fi
}


function redirsuccess()
{
	echo "Status: 302"
	echo "Content-type: text/html"
	echo "Location: $SCRIPT_NAME?scsfl=$1"
	echo
}
function redirerror()
{
        echo "Status: 302"
	echo "Content-type: text/html"
        echo "Location: $SCRIPT_NAME?error=$1"
        echo
}


command=${QUERY_STRING:0:5}
ipaddr=${QUERY_STRING:6}

if [ "${command}" != "" ]; then
        case "$command" in
        [e][n][a][b][l])
                enableppm;;
        [d][i][s][a][b])
                disableppm;;
	[e][r][r][o][r])
		;;
	[s][c][s][f][l])
		;;
                *)
                redirerror "Entered unknown command";;
        esac
fi


echo "Content-Type: text/html"
echo

cat header
echo "<div class=\"col_12\">"

echo "<div class=\"col_6\">"
echo "<h4>Switch PPM state:</h3>"
cat form

if [ "${command}" != "" ]; then
        case "$command" in
        [e][r][r][o][r])
                error $ipaddr;;
        [s][c][s][f][l])
                success $ipaddr;;
                *)
                error "Entered unknown command $command";;
        esac
fi

echo "</div>"



echo "<div class=\"col_6\">"
echo "<h4>Current PPM status:</h3>"
echo "<ul>"
#/sbin/ip route list table asset |grep unreachable |grep -Eo '([0-9.]{0,16}[0-9.])'|xargs -n 1 -i echo "<li>{} <a href=\"?delip={}\"><span class=\"icon small red\" data-icon=\"x\"></span></a></li>" # Magic!

up=$(statapp -s mgmt|grep UP)
if [ "${up}" != "" ]; then
 	echo "PPM is UP"      
fi

up=$(statapp -s mgmt|grep DOWN)
if [ "${up}" != "" ]; then
        echo "PPM is DOWN"
fi

up=$(statapp -s mgmt|grep PARTIAL)
if [ "${up}" != "" ]; then
        echo "PPM is STARTING, WAIT!"
fi

echo "</ul>"
echo "</div>"

