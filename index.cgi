#!/bin/bash

function valid_ip()     ## Validate intered IP address
{
        local  result=1;        ## false

        if [[ $ipaddr =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]]; then
        {
                OIFS=$IFS
                IFS='.'
                ip=($ip)
                IFS=$OIFS
                [[ ${ip[0]} -le 255 && ${ip[1]} -le 255  && ${ip[2]} -le 255 && ${ip[3]} -le 255 ]]
                result=$?
        }
        fi
                return $result;
}

function error()
{
        echo "<div class=\"notice error\">"
        echo "<span class=\"icon medium\" data-icon=\"L\"></span>" #icon
        msg="${1//%20/ }"
	echo "${msg//%27/\`}"
        echo "<a href=\"#close\" class=\"icon close\" data-icon=\"x\"></a>"
        echo "</div>"
}

function success()
{
        echo "<div class=\"notice success\">"
        echo "<span class=\"icon medium\" data-icon=\"K\"></span>" #icon
        echo "${1//%20/ }"
        echo "<a href=\"#close\" class=\"icon close\" data-icon=\"x\"></a>"
        echo "</div>"
}

function addip()
{
	curdate=$(date +%d.%m.%y\ %H:%M)
        if(valid_ip $1) then
        {
                /sbin/ip route add unreachable $1 table asset     
		if [ $? -ne 0 ]; then
		  redirerror "Error!"
  		  echo "$curdate Client ip $REMOTE_ADDR ; add ip $1" >> errlog.txt
		else
		  redirsuccess "ip address $1 have been added to the table asset"
		  echo "$curdate Client ip $REMOTE_ADDR ; add ip $1" >> log.txt
		fi
        }
        else
		redirerror "You've entered incorrect IP address"
		echo "$curdate Client ip $REMOTE_ADDR ; add ip $1" >> errlog.txt
        fi
}

function delip()
{
	curdate=$(date +%d.%m.%y\ %H:%M)
        if(valid_ip $1) then
        {
                /sbin/ip route delete unreachable $1 table asset 
		if [ $? -ne 0 ]; then
                  redirerror "Error!"
		  echo "$curdate Client ip $REMOTE_ADDR ; del ip $1" >> errlog.txt
                else
                  redirsuccess "ip address $1 have been deleted from the table asset"
		  echo "$curdate Client ip $REMOTE_ADDR ; del ip $1" >> log.txt
                fi

        }
        else
                redirerror "You've entered incorrect IP address"
		echo "$curdate Client ip $REMOTE_ADDR ; del ip $1" >> errlog.txt
        fi
}

function redirsuccess()
{
	echo "Status: 302"
	echo "Content-type: text/html"
	echo "Location: $SCRIPT_NAME?scsfl=$1"
	echo
}
function redirerror()
{
        echo "Status: 302"
	echo "Content-type: text/html"
        echo "Location: $SCRIPT_NAME?error=$1"
        echo
}


command=${QUERY_STRING:0:5}
ipaddr=${QUERY_STRING:6}

if [ "${command}" != "" ]; then
        case "$command" in
        [a][d][d][i][p])
                addip $ipaddr;;
        [d][e][l][i][p])
                delip $ipaddr;;
	[e][r][r][o][r])
		;;
	[s][c][s][f][l])
		;;
                *)
                redirerror "Entered unknown command";;
        esac
fi


echo "Content-Type: text/html"
echo

cat header
echo "<div class=\"col_12\">"

echo "<div class=\"col_6\">"
echo "<h4>Add new unreachable address:</h3>"
cat form

if [ "${command}" != "" ]; then
        case "$command" in
        [e][r][r][o][r])
                error $ipaddr;;
        [s][c][s][f][l])
                success $ipaddr;;
                *)
                error "Entered unknown command $command";;
        esac
fi

echo "</div>"



echo "<div class=\"col_6\">"
echo "<h4>List of unreachable ip addresses:</h3>"
echo "<ul>"
/sbin/ip route list table asset |grep unreachable |grep -Eo '([0-9.]{0,16}[0-9.])'|xargs -n 1 -i echo "<li>{} <a href=\"?delip={}\"><span class=\"icon small red\" data-icon=\"x\"></span></a></li>" # Magic!
echo "</ul>"
echo "</div>"

